package com.example.demo;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@WebMvcTest(HelloController.class)

public class HelloControllerTests {

    @Autowired
    MockMvc mockMvc;

    @Test
    void sayHello_noParam_rtnHelloWorld() throws Exception {
        // /hello string "Hello World"
        // Arrange
        // Act
        mockMvc.perform(get("/hello"))
               //Assert
               .andExpect(status().isOk())
               .andExpect(content().string("Hello World"));
    }

    @Test
    void sayHello_myName_rtnHelloName() throws Exception {
        mockMvc.perform(get("/hello?name=Matthias"))
               .andExpect(status().isOk())
               .andExpect(content().string("Hello Matthias"));
    }

    @Test
    void getTwoNumbersReturnSum() throws Exception {
        mockMvc.perform(get("/add")
               .param("firstNumber", "5")
               .param("secondNumber", "4"))
               .andExpect(status().isOk())
               .andExpect(content().string("9"));
    }

}
