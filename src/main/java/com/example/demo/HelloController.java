package com.example.demo;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

//    @GetMapping("/hello")
//    public String sayHello(){
//        return "Hello World";
//    }

    @GetMapping("/hello")
    public String sayHello(@RequestParam(defaultValue = "World", required = false) String name) {
        return String.format("Hello %s", name);
    }

    @GetMapping("/add")
    public String addNumbers(@RequestParam(required = true) int firstNumber,@RequestParam(required = true) int secondNumber) {
        return String.valueOf(firstNumber + secondNumber);
    }

}
